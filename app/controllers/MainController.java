package controllers;

import play.mvc.*;
import play.data.Form;

import formdata.AuthenticationData;

import models.User;

public class MainController extends Controller {

	public static Result login() {
		return ok(views.html.login.render(Form.form(AuthenticationData.class)));
	}

	public static Result login_submit() {
		Form<AuthenticationData> form = Form.form(AuthenticationData.class).bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(views.html.login.render(form));
		} else {
			AuthenticationData data = form.get();
			User user = User.byLogin(data.username, data.password);
			if (user == null) {
				return redirect(routes.MainController.login());
			}
			
			return ok(views.html.index.render());
		}
	}

}
