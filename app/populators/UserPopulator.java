package populators;

import play.mvc.*;

import models.User;

public class UserPopulator extends Controller {
	
	public static final String DUMMY_USERNAME = "testuser";
	public static final String DUMMY_PASSWORD = "bestpassword";
	
	public static Result populate() {
		User user = User.byUsername(DUMMY_USERNAME);
		if (user == null) {
			User.create(new User(DUMMY_USERNAME, DUMMY_PASSWORD));
		}
		
		return ok("Done! Users populated.");
	}

}
