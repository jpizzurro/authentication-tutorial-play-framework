package controllers;

import play.mvc.*;

import models.User;

public class MobileController extends Controller {
	
	public static Result authenticate(String u, String p) {
		User user = User.byLogin(u, p);
		if (user != null) {
			return ok("success");
		}
		
		return ok("failure");
	}

}
