package formdata;

import models.User;

public class AuthenticationData {

	public String username;
	public String password;

	public String validate() {
		User user = User.byLogin(username, password);
		if (user == null) {
			return "Invalid username and/or password! Please try again.";
		}

		return null;
	}

}
