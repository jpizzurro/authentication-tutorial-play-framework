package models;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;

import formdata.AuthenticationData;

@Entity
public class User extends Model {

	private static final long serialVersionUID = -229664861034677724L;

	// Fields

	@Id
	public Long id;

	@Required
	@Column(unique = true)
	public String username;

	@Required
	public String password;	// TODO: hash passwords!

	@CreatedTimestamp
	public Timestamp createdTime;

	@UpdatedTimestamp
	public Timestamp updatedTime;


	// Constructors

	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public User(AuthenticationData data) {
		this.username = data.username;
		this.password = data.password;
	}


	// Functions and stuff

	public static Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);

	public static List<User> all() {
		return find.where().ne("disavowed", true).findList();
	}

	public static User create(User user) {
		user.save();
		return user;
	}

	public static User byId(Long id) {
		return find.where().eq("id", id).findUnique();
	}

	public static User byId(String idString) {
		if (idString == null) {
			return null;
		}

		if (idString.equals("")) {
			return null;
		}

		return byId(Long.parseLong(idString));
	}

	public static User byUsername(String username) {
		return find.where().eq("username", username).findUnique();
	}

	public static User byLogin(String username, String password) {
		User user = byUsername(username);
		if (user == null) {
			return null;
		}

		// TODO: hash passwords!
		if (!user.password.equals(password)) {
			return null;
		}

		return user;
	}

}